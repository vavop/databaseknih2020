package cz.vavop.databazeknih2020;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class VypisKnihAdapter extends BaseAdapter {

    private Context context;
    private Kniha[] items;

    public VypisKnihAdapter(Context c, Kniha[] values) {
        super();
        context = c;
        items = values;
    }


    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_kniha, parent, false);

        /// polozky
        ((TextView)rowView.findViewById(R.id.rowNazev)).setText(items[position].nazev);
        ((TextView)rowView.findViewById(R.id.rowAutor)).setText(items[position].autor);
        ((TextView)rowView.findViewById(R.id.rowPocet)).setText(String.valueOf(items[position].pocetStran));
        ((TextView)rowView.findViewById(R.id.rowId)).setText(String.valueOf(items[position].id));
        Button butt = rowView.findViewById(R.id.rowPrecist);
        butt.setTag(items[position].id);
        butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // pokud se podari odstranit zaznam z DB, odstrani se take cely radek na obrazovce
                Intent i = new Intent(context, KnihadetailActivity.class);
                i.putExtra("idknihy", (int)v.getTag());
                context.startActivity(i);
            }
        });

        return rowView;
    }
}
