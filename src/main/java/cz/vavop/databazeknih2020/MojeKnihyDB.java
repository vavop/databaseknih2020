package cz.vavop.databazeknih2020;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by grutier on 15.4.2015.
 */
public class MojeKnihyDB {
	static final String DATABASE_NAME = "MojeKnihy";
	static final int DATABASE_VERSION = 2;

	static final String DATABASE_TABLE = "knihy";
	static final String KEY_ID = "_id";
	static final String KEY_NAZEV = "nazev";
	static final String KEY_POCETSTRAN = "pocstran";
	static final String KEY_AUTOR = "autor";

	static final String TAG = "VAVOP MojeKnihyDB";


	private static final String DATABASE_CREATE = "create table " + DATABASE_TABLE + " (_id integer primary key autoincrement, " +
			KEY_NAZEV+" text not null, "+KEY_POCETSTRAN+" integer not null, "+KEY_AUTOR+" text);";

	private static final String DATABASE_DROP_ENTRIES = "DROP TABLE IF EXISTS " + DATABASE_TABLE;

	final Context context;
	MujDatabaseHelper DBHelper;
	SQLiteDatabase db;

	/**
	 * Konstruktor => nastavení DB
	 */
	public MojeKnihyDB(Context ctx)	{
		this.context = ctx;
		DBHelper = new MujDatabaseHelper(this.context);
	}

	private static class MujDatabaseHelper extends SQLiteOpenHelper {

		MujDatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase sqlDB) {
			sqlDB.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase sqlDB, int oldVersion, int newVersion) {
			/// provedení potřebných změn///
			if(oldVersion < 2) {
				Log.d(TAG, "Updating table from " + oldVersion + " to " + newVersion);
				String query = "ALTER TABLE " + DATABASE_TABLE + " ADD COLUMN " + KEY_AUTOR +" text";
				sqlDB.execSQL(query);
			}
		}
	}


	//--- otevření DB ---
	public MojeKnihyDB open() throws SQLException	{
		db = DBHelper.getWritableDatabase();
		Log.d(TAG, "Otvírám DB verzi " + db.getVersion());
		return this;
	}
	//--- zavøení DB ---
	public void close() {
		DBHelper.close();
	}

	//--- vložit záznam ---
	public long vlozKnihu(Kniha kniha) {
		ContentValues vstupniHodnoty = new ContentValues();
		vstupniHodnoty.put(KEY_NAZEV, kniha.nazev);
		vstupniHodnoty.put(KEY_POCETSTRAN, kniha.pocetStran);
		vstupniHodnoty.put(KEY_AUTOR, kniha.autor);
		return db.insert(DATABASE_TABLE, null, vstupniHodnoty);
	}
	//--- smazání konkrétního záznamu ---
	public boolean smazKnihu(int id) {
		// TODO //
		String where = KEY_ID + " = ?";
		if(db == null) {
			this.open();
		}
		int res = db.delete(DATABASE_TABLE, where, new String[] {String.valueOf(id)});
		Log.d("VAVOP MojeKnihyDB", "vysledek: " + res);
		if(res > 0) {
			return true;
		}else {
			return false;
		}
	}

	//--- smazání všech záznamù ---
	public int smazKnihy() {
		// TODO //
		return 0;
	}

	//--- získat vsechny knihy ---
	public Kniha[] vsechnyKnihy(String order) {
		if(order == "pocetStran") {
			order = KEY_POCETSTRAN;
		}else if(order == "autor") {
				order = KEY_AUTOR;
		}else {
			order = KEY_NAZEV;
		}

		/// nerozlisovani malych a VELKYCH znaku ///
		order += " COLLATE NOCASE";

		Log.d("VAVOP", order);
		/// parametry metody: tabulka / atributy-sloupce / podmínka / argumenty podmínky / group / having / order / limit
		Cursor data = db.query(DATABASE_TABLE, new String[]{KEY_ID, KEY_NAZEV, KEY_POCETSTRAN, KEY_AUTOR}, null, null, null, null, order);

		if(data.getCount() < 1) {
			return new Kniha[0];
		}

		Kniha[] knihy = new Kniha[data.getCount()];
		if (data.moveToFirst()) {
			do {
				knihy[data.getPosition()] = new Kniha(data.getInt(0), data.getString(1), data.getInt(2), data.getString(3));
			} while (data.moveToNext());
		}
		return knihy;
	}

	//--- získat jeden záznam ---
	public Kniha jednuKnihu(int rowId) throws SQLException {
		// TODO //
		//return mCursor;
		Cursor data = db.query(DATABASE_TABLE,
				new String[]{KEY_ID, KEY_NAZEV, KEY_POCETSTRAN, KEY_AUTOR},
				KEY_ID + " = " + rowId, null, null, null, null);
		if(data.moveToFirst()) {
			return new Kniha(data.getInt(0), data.getString(1), data.getInt(2), data.getString(3));
		}
		return null;
	}
}