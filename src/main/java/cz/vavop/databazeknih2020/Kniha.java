package cz.vavop.databazeknih2020;

public class Kniha {
    String nazev = "";
    String autor = "";
    int pocetStran = 0;
    int id = -1;

    public Kniha(int id, String n, int ps, String a) {
        nazev = n;
        autor = a;
        pocetStran = ps;
        this.id = id;
    }

    public Kniha(String n, int ps, String a) {
        nazev = n;
        autor = a;
        pocetStran = ps;
    }

}
