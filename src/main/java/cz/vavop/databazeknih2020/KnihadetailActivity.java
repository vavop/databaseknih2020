package cz.vavop.databazeknih2020;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class KnihadetailActivity extends Activity {

    private static String LOG_TAG = "VAVOP DetailKnihy";
    private MojeKnihyDB db;
    private Kniha kniha = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knihadetail);

        this.db = new MojeKnihyDB(this);

        Intent volan = getIntent();
        int id = volan.getIntExtra("idknihy", -1);
        if( id >= 0) {
            this.nactiKnihu(id);
            Toast.makeText(this, "Knihu s ID " + id, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Není kniha", Toast.LENGTH_LONG).show();
            goBack();
        }

    }

    private void goBack() {
        Intent i = new Intent(this,KnihyActivity.class);
        startActivity(i);
    }

    private void nactiKnihu(int id) {
        db.open();
        kniha = db.jednuKnihu(id);
        db.close();
        if(!kniha.equals(null)) {
            ((TextView) findViewById(R.id.rowNazev)).setText(kniha.nazev);
            ((TextView) findViewById(R.id.rowAutor)).setText(kniha.autor);
            ((TextView) findViewById(R.id.rowPS)).setText("Počet stran: " + kniha.pocetStran);
            ((TextView) findViewById(R.id.rowId)).setText("ID: " + String.valueOf(kniha.id));
        }
    }


    public void smazatKnihu(View view) {
        if(!kniha.equals(null)) {
            db.open();
            if(db.smazKnihu(kniha.id)) {
                Toast.makeText(this, "Kniha smazána", Toast.LENGTH_LONG).show();
                goBack();
            };
            db.close();
        }
    }
}
