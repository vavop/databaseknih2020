package cz.vavop.databazeknih2020;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.List;

public class KnihyActivity extends Activity {
	private EditText nazev;
	private EditText ps;
	private EditText autor;
	private ListView vypis;
	private TextView celkem;
	private Switch razeni;
	private static String LOG_TAG = "VAVOP KnihyActivity";
	private int pocetKnih = 0;

	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = this;

		setContentView(R.layout.activity_knihy);

		this.nazev = findViewById(R.id.nazevKnihy);
		this.ps = findViewById(R.id.pocetStran);
		this.autor = findViewById(R.id.autor);
		this.vypis = findViewById(R.id.seznamKnih);
		this.celkem = findViewById(R.id.celkemKnih);
		this.razeni = findViewById(R.id.razeniVypisu);
		razeni.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				vypisKnihy();
			}
		});

		vypisKnihy();
	}

	private void naplnSeznam(Kniha[] data) {

	}

	/**
	 * metoda volaná tlačítkem v layoutu
	 * @param view
	 */
	public void pridejZaznam(View view) {
		if(nazev.getText().length() > 0 && ps.getText().length() > 0) {
			MojeKnihyDB db = new MojeKnihyDB(this);
			db.open();
			long id = db.vlozKnihu(new Kniha(nazev.getText().toString(), Integer.parseInt(ps.getText().toString()), autor.getText().toString()));
			if(id < 0) {
				Toast.makeText(this, "Nepodařilo se vložit záznam!", Toast.LENGTH_SHORT);
			}
			db.close();
			nazev.setText("");
			ps.setText("");
			vypisKnihy();
			//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		} else {
			Toast.makeText(this, "Vyplň údaje!", Toast.LENGTH_SHORT).show();
		}
	}

	public void vypisKnihy() {
		MojeKnihyDB db = new MojeKnihyDB(this);
		db.open();
		Kniha[] data;
		if(razeni.isChecked()) {
			data = db.vsechnyKnihy("autor");
		}else {
			data = db.vsechnyKnihy("nazev");
		}
		db.close();

		final VypisKnihAdapter adapter = new VypisKnihAdapter(this, data);

		vypis.setAdapter(adapter);
		pocetKnih = data.length;
		setCelkemText();

	}

	private void setCelkemText() {
		celkem.setText("Celkem knih: " + this.pocetKnih);
	}


	/**
	 * @param data - Cursor - 0 id, 1 nazev, 2 pocet stran
	 */
	private ConstraintLayout pridejRadek2(Cursor data) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ConstraintLayout rowView = (ConstraintLayout) inflater.inflate(R.layout.row_kniha, vypis, false);

		TextView id = rowView.findViewById(R.id.rowId);
		id.setText(data.getString(0));

		TextView n = rowView.findViewById(R.id.rowNazev);
		n.setText(data.getString(1));

		TextView a = rowView.findViewById(R.id.rowAutor);
		a.setText(data.getString(2));

		Button sm = rowView.findViewById(R.id.rowSmazat);
		sm.setTag(data.getInt(0));
		sm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(LOG_TAG, "id zaznamu ke smazani: " + v.getTag().toString());
				// pokud se podari odstranit zaznam z DB, odstrani se take cely radek na obrazovce
				if(smazKnihu(Integer.parseInt(v.getTag().toString()))) {
					ConstraintLayout l = (ConstraintLayout) v.getParent();
					vypis.removeView(l);
					pocetKnih--;
					setCelkemText();
				}
			}
		});

		Button cist = rowView.findViewById(R.id.rowPrecist);
		cist.setTag(data.getInt(0));
		cist.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(LOG_TAG, "otviram knihu s id: " + v.getTag().toString());
				// pokud se podari odstranit zaznam z DB, odstrani se take cely radek na obrazovce
				Intent i = new Intent(context, KnihadetailActivity.class);
				i.putExtra("idknihy", (int)v.getTag());
				context.startActivity(i);
			}
		});
		return rowView;
	}

	private boolean smazKnihu(int id) {
		MojeKnihyDB db = new MojeKnihyDB(this);
		boolean res = db.smazKnihu(id);
		if(res) {
			Toast.makeText(this, "Mažu knihu " + id, Toast.LENGTH_SHORT).show();
		}else {
			Toast.makeText(this, "Nelze smazat knihu " + id, Toast.LENGTH_LONG).show();
		}
		return res;
	}
}